<?php
$statsFile = 'S:\temp\TEMPerX1.csv';
$contents = file_get_contents($statsFile);
file_put_contents($statsFile, '');
$lines = explode("\n", $contents);

$counter = 0;
foreach ($lines as $line){
    if (empty($line)) {
        continue;
    }
    $cols = explode(',', $line);
    if ($cols[0] == 'Time') {
        continue;
    }
    $url = 'https://wendy.greenbury.co.uk/stat/store/temperature/';
    $url .= intval($cols[1]).'/';
    $url .= 'hall-lane-office/';
    $url .= strtotime($cols[0]) - 3600;
    file_get_contents($url);
    echo 'Done: '.$counter++."        \r";
}
