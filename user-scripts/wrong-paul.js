// ==UserScript==
// @name         WrongPaul
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Stop Sami selecting the wrong Paul
// @author       You
// @match        https://www.fastmail.com/mail/compose*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    setTimeout(function(){
        var to = document.querySelector('.s-compose-to textarea');
        if (!to || !to.addEventListener) {
            alert("Could not find To field. WrongPaul TamperMonkey script can't run");
            console.log("Could not find To field. WrongPaul TamperMonkey script can't run");
        } else {
            to.addEventListener('keyup', function(event){
                var text = this.value;
                var hasWls = text.contains('@wls.org.uk');
                var hasPaulSouthgate = text.contains('Southgate');
                if(hasWls && hasPaulSouthgate) {
                    alert('Wrong Paul!');
                }
            });
        }
    }, 1000);
})();