<?php
if(!file_exists($clone_config['clone_path'].'/public/wp-config-local.php')) {
	return;
}
require $clone_config['clone_path'].'/public/wp-config-local.php';
if (defined('DB_NAME')) {
	$dbname = DB_NAME;
}else if (isset($localConfig['DB_NAME'])) {
	$dbname = $localConfig['DB_NAME'];
} else {
	return;
}
$cmd = 'mysql -uwp_user -e '.escapeshellarg("CREATE DATABASE ".$dbname);
`$cmd`;
$import_script = $clone_config['clone_path'].'/scripts/import_database.php';
if(file_exists($import_script)) {
	`php $import_script`;
}
