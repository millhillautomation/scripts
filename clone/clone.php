<?php
if (!isset($argv[1])) {
	echo <<<DOCS
Clones a git repo locally, creates a vhost file and adds a record to the system hosts file.

Syntax:
	clone repo_url [name]
where repo_url is the clone string for git. The final part (less .git) will be used as the name if
`name` is not specified' e.g git@bitbucket.org/user/some-repo.git will be 'some-repo'. Override
this by passing the second parameter.
DOCS;
	die;
}

$config = [
	'clone_path' => 'c:\wamp\www',
	'vhost_path' => 'c:\wamp\vhosts',
	'vhost_template' => __DIR__.'\vhost.template.config',
	'hosts_path' => 'c:\windows\System32\drivers\etc\hosts',
];

// Work out the variables for our current clone
$url = $argv[1];
if (isset($argv[2])) {
	$name = $argv[2];
} else {
	$urlParts = [];
	preg_match('_/(.+).git$_', $url, $urlParts);
	$name = $urlParts[1];
}
$clone_config = [
	'clone_path' => $config['clone_path'].'/'.$name,
	'vhost_path' => $config['vhost_path'].'/'.$name.'.conf',
	'url' => 'local.'.$name,
	'hosts_string' => "\r\n127.0.0.1       local.{$name}\r\n",
];

if (file_exists($clone_config['clone_path']) === false) {
	$cmd = 'git clone '.escapeshellarg($url).' '.escapeshellarg($clone_config['clone_path']);
	$result = exec($cmd);
}

$vhost = file_get_contents($config['vhost_template']);
$variables = [
	'{{ name }}' => $name,
	'{{ url }}' => $clone_config['url'],
	'{{ repo_path }}' => $clone_config['clone_path'],
];
$vhost = str_replace(array_keys($variables), array_values($variables), $vhost);
file_put_contents($clone_config['vhost_path'], $vhost);

$hosts = file_get_contents($config['hosts_path']);
if(strpos($hosts, $clone_config['hosts_string']) === false) {
	file_put_contents($config['hosts_path'], trim($hosts).$clone_config['hosts_string']);
}

// Check the Logs directory exists
if(!file_exists($clone_config['clone_path'].'/logs')) {
	mkdir($clone_config['clone_path'].'/logs');
}

require __DIR__.'/create-wp-database.php';

`toast "Finished cloning {$name}"`;
