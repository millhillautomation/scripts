@echo off
setx /M PATH "%PATH%;S:\bin\scripts;S:\jeeves\sami\bin;w:\bin\php7.4;"

# @rem Add some other paths
setx /M PATH "%PATH%;S:\Dropbox\Jeeves\Sami\Bin\KiTTy\"
setx /M GIT_SSH "S:\Dropbox\Jeeves\Sami\Bin\KiTTy\PLINK.EXE"

mkdir c:\temp\

# @rem Install Scoop
Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://get.scoop.sh')

# @rem install dev stuff
scoop install git
npm install -g browser-sync

# @rem Not doing this because I haven't configured the ini yet. One day... scoop install git
scoop install mariadb
scoop install nodejs
scoop install composer
scoop install bower
scoop install docker
scoop install docker-compose
scoop install wp-cli
scoop install time

npm install -g cross-env

# # @rem Install other open source things
scoop install 7zip

# @rem Install things to make me think I'm on Linux
scoop install gzip
scoop install grep
scoop install touch
scoop install which


# @rem Install non-portable/closed source stuff
scoop install teamviewer-np
scoop install dropbox-np
scoop install microsoft-office-np

<<<<<<< HEAD
@rem "You need to do this in wsl too:"
@rem "From: https://docs.microsoft.com/en-us/windows/dev-environment/javascript/nodejs-on-wsl : "
// Install NPM
@rem curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
@rem bash
@rem nvm install --lts

@rem "And install a bunch of ubuntu/php things"
@rem // npm needs 'make' and friends
@rem sudo apt install build-essential

@rem // Install PHP7 and symlink it so we can use old php too
@rem sudo apt install php7.4
@rem sudo apt install php7.4-mysql php7.4-mbstring php7.4-xml php7.4-gd php7.4-curl
@rem sudo ln -s /usr/bin/php7.4 /usr/bin/php7

@rem sudo apt install php8.1
@rem sudo apt install php8.1-mysql php8.1-mbstring php8.1-xml php8.1-gd php8.1-curl php8.1-zip
=======
bash install.sh
>>>>>>> dca7eab9bce1db3be5b01fa7aee71a0067cb756b
