<?php

// Get the latest version of the hosts file
copy('https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts', __DIR__.'/bin/hosts-block-adware.txt');

// Clear the cache and restart the DNS Server
`"C:\Program Files (x86)\Acrylic DNS Proxy\AcrylicController.exe" PurgeAcrylicCacheDataSilently`;
