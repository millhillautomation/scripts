<?php
    $dir = 'd:\shared\sage';
?>
<html>
<head>
    <title>D and N Sage Company Lists</title>
</head>
<body>
<h1>Sage companies found in <?=htmlspecialchars($dir, ENT_QUOTES)?></h1>
<table>
<thead>
    <tr>
        <th>Folder Name</th>
        <th>Company Name</th>
    </tr>
</thead>
<tbody>
<?php
$allCompanies = glob($dir.'\*');
foreach ($allCompanies as $company) {
    $companyName = '';
    if (!file_exists($company.'\ACCDATA\SETUP.DTA')) {
        $companyName = 'Invalid Data';
    } else {
        $setupdta = file_get_contents($company.'\ACCDATA\SETUP.DTA');
        $parts = explode(chr(0), $setupdta);
        $companyName = substr($parts[1], 1);
    }
    echo '<tr>';
    echo    '<td>'.htmlspecialchars(basename($company), ENT_QUOTES).'</td>';
    echo    '<td>'.htmlspecialchars($companyName, ENT_QUOTES).'</td>';
    echo '</tr>';
}
?>
</tbody>
</table>
</body>
</html>
