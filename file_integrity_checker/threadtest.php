<?php

class WebRequest extends Thread {
    public $url;
    public $response;
     
    public function __construct($url){
        $this->url = $url;
    }
     
    public function run() {
        $this->response = file_get_contents($this->url);
    }
}
 
$request = new WebRequest("http://pthreads.org");
 
if ($request->start()) {
     
    /* do some work */
     
    /* ensure we have data */
    $request->join();
     
    /* we can now manipulate the response */
    var_dump($request->response);
}
