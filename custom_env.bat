@echo off

setx /M GIT_SSH S:\Jeeves\Sami\Bin\KiTTy\PLINK.EXE
setx /M PATH "%PATH%;S:\bin\scripts"
setx /M PATH "%PATH%;C:\wamp\bin\mysql\mysql5.6.17\bin"
setx /M PATH "%PATH%;C:\wamp\bin\php\php5.5.12"
setx /M PATH "%PATH%;S:\Jeeves\Sami\Bin\KiTTy"

setx /M WP_ENV "development"