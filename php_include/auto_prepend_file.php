<?php
if (isset($_GET['phpinfo'])) {
	phpinfo();
	die;
}
if (!file_exists(__DIR__.'/../vendor/autoload.php')) {
	echo "Please run `composer install` in the scripts dir (close to ".__DIR__.")\n";
	die;
} else {
	require __DIR__.'/../vendor/autoload.php';

}

function is_cli()
{
    if(
    	(
    		empty($_SERVER['REMOTE_ADDR'])
    		and !isset($_SERVER['HTTP_USER_AGENT'])
    	)
    	|| (
    		isset($_SERVER['argv'])
    		&& count($_SERVER['argv']) > 0
    	)
    ) {
        return true;
    }

    return false;
}

if(is_cli()) {
	function fb()
	{
		// Make the text red
		fwrite(STDOUT, "\033[0;31m");
		$args = func_get_args();
		switch (count($args)) {
			case 1:
				fb('LOG', $args[0]);
				break;
			case 2:
				if (isset($args[0])) {
					$args[0] = json_encode($args[0], JSON_PRETTY_PRINT);
				}
				if (isset($args[1])) {
					$args[1] = json_encode($args[1], JSON_PRETTY_PRINT);
				}

				fwrite(STDOUT, $args[0].': '.$args[1]."\n");
				break;
			default:
				foreach ($args as $arg) {
					fb($arg);
				}
		}
		fwrite(STDOUT, "\e[0m");
	}
} else {
	// Give everything access to fb()
	if ( ! is_callable('fb')) {
		function fb() {
			$chromePhp = ChromePhp::getInstance(true);
			$FirePHPInstance = FirePHP::getInstance(true);

			$args = func_get_args();
			// Look for "empty" variables and turn them into something
			// Chrome will actually display. This is mainly for NULL values.
			foreach($args as &$argument) {
				if (empty($argument)) {
					$argument = var_export($argument, true);
				}
			}
			if ( isset($chromePhp) ){
				call_user_func_array(array($chromePhp, 'log'), $args);
			}
			if ( isset($FirePHPInstance) ){
				call_user_func_array(array($FirePHPInstance, 'fb'), func_get_args());
			}
			// var_dump(func_get_args());
		}
	}
}

if (!is_cli()) {
	ob_start();
}

$auto_prepend_script_time_start = microtime(true);
