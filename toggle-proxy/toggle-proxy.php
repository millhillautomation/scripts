<?php

$cmd = [];
$cmd[] = 'reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings" /v ProxyEnable /t REG_DWORD /f /d ';

if (isset($argv[1]) && strtolower($argv[1]) === 'enable') {
	$cmd[] = '1';
	echo "Enabling Proxy\n";
} else {
	$cmd[] = '0';
	echo "Disabling Proxy\n";
}
$final_cmd = implode($cmd);
`$final_cmd`;
$powershell_cmd = 'powershell.exe -Version 2 -PSConsoleFile '.escapeshellarg(__DIR__.'/power-shell-send-setting-change.psc1');
// `$powershell_cmd`;
