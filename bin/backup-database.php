<?php
$host = '192.168.1.240';
$user = 'root';
$pass = '';
$charset = 'utf8mb4';
$backupRoot = 'x:\\dropbox\Backups\backup-sql\\'; // With (escaped) trailing slash
if (!file_exists($backupRoot)) {
    $backupRoot = '/mnt/s/backup-sql/'; // Perhaps we're in WLS
}
if (!file_exists($backupRoot)) {
    echo "Backup Root not found: " . $backupRoot . "\n";
    exit;
}
$dateTimeFormat = 'Y-m-d_H-i-s';
$now = date($dateTimeFormat); // The timestamp to use for this backup run
$todaysBackupRoot = $backupRoot . $now . DIRECTORY_SEPARATOR;
// Make sure todays folder exists.
mkdir($todaysBackupRoot, 0777, true);
$skipDbs = ['information_schema', 'mysql', 'performance_schema'];

$dsn = "mysql:host=$host;charset=$charset";
$options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
try {
    $pdo = new PDO($dsn, $user, $pass, $options);
} catch (\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int)$e->getCode());
}

$dbs = $pdo->query('show databases');

foreach ($dbs as $db) {
    $db = $db['Database'];
    if (in_array($db, $skipDbs)) {
        continue;
    }
    $file = $todaysBackupRoot . $db . '.sql';
    $dumpCommand = "mysqldump -u {$user} -h {$host} $db >> $file"; // Note the append command
    echo "Dumping $db to " . $file . "\n";
    file_put_contents($file, "CREATE DATABASE IF NOT EXISTS `$db`;\nUSE `$db`;\n");
    $compressCommand = "gzip -9 $file";
    `$dumpCommand`;
    echo "Compressing $file\n";
    `$compressCommand`;
}

$previousBackupPaths = glob($backupRoot . DIRECTORY_SEPARATOR . '*');
// If we have more than 5 backups, delete any older than 7 days.
if ((!isset($doNotClean) || $doNotClean) && count($previousBackupPaths) > 5) {
    echo "Pruning old backups\n";
    foreach ($previousBackupPaths as $previousBackupPath) {
        $name = basename($previousBackupPath);
        $timestamp = \DateTime::createFromFormat($dateTimeFormat, $name);
        if (!$timestamp) {
            echo "Unable to parse previous backup: $previousBackupPath. Invald date format: $name\n";
            continue;
        }
        if ((time() - $timestamp->getTimestamp()) > 60 * 60 * 24 * 30) {
            echo "Deleting this backup for being more than 30 days old: " . $previousBackupPath . "\n";

            $it = new RecursiveDirectoryIterator($previousBackupPath, FilesystemIterator::SKIP_DOTS);
            $it = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
            foreach ($it as $file) {
                if ($file->isDir()) rmdir($file->getPathname());
                else unlink($file->getPathname());
            }
            rmdir($previousBackupPath);
        }
    }
}
