<?php
// Run a backup before we start, and also import some variables. Sneaky.
echo "Creating a backup before we restore...\n";
$doNotClean = true; // Don't prune backups while restoring
require_once __DIR__ . '/backup-database.php';

$restoreNow = '2020-10-08_14-05-07'; // The folder name to restore from.
$todaysBackupSource = $backupRoot . $restoreNow;


// Make sure the folder exists.
if (!file_exists($todaysBackupSource)) {
    echo "Path to backup does not exist: $todaysBackupSource\n";
    exit;
}
$todaysBackupRoot = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'restoredb_' . $restoreNow . '-' . microtime(true) . DIRECTORY_SEPARATOR;
echo "Working directory: $todaysBackupRoot\n";
// Copy the backup - lets not work on the real one.
$copyCmd = 'xcopy '.escapeshellarg($todaysBackupSource).' '. escapeshellarg($todaysBackupRoot);
$copyOutput = `$copyCmd`;

if (!file_exists($todaysBackupRoot)) {
    echo "ERROR: Something went wrong copying the backup we want to restore to the temporary location\n";
    echo "Command: $copyCmd\n";
    echo "Message: $copyOutput\n";
    exit;
}

// Extract all the gzip files
$it = new RecursiveDirectoryIterator($todaysBackupRoot, FilesystemIterator::SKIP_DOTS);
$it = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
foreach ($it as $file) {
    echo "Extracting $file\n";
    $decompressCommand = 'gzip -df ' . escapeshellarg($file);
    `$decompressCommand`;
}

// Import all the .sql files
$it = new RecursiveDirectoryIterator($todaysBackupRoot, FilesystemIterator::SKIP_DOTS);
$it = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
foreach ($it as $file) {
    echo "Restoring $file\n";
    $importCommand = "mysql -u {$user} -h {$host} -e " . escapeshellarg('SOURCE ' . $file);
    `$importCommand`;
    // Delete the file
    unlink($file);
}
// This should be empty now
rmdir($todaysBackupRoot);
