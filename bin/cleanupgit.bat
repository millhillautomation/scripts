for D in *; do
    if [ -d "${D}" ]; then
        echo "${D}"   # your processing here
    fi
done

exit;

cd w:\www\acacia-cottages\
git prune
git gc
git fetch

cd w:\www\ape-snacks\
git prune
git gc
git fetch

cd w:\www\hiddenpause\
git prune
git gc
git fetch

cd w:\www\orchid-city-spa\
git prune
git gc
git fetch

cd w:\www\sage\
git prune
git gc
git fetch

cd w:\www\sudzy\
git prune
git gc
git fetch

cd w:\www\tests\
git prune
git gc
git fetch

cd w:\www\untitled\
git prune
git gc
git fetch

cd w:\www\wamplangues\
git prune
git gc
git fetch

cd w:\www\wampthemes\
git prune
git gc
git fetch

cd w:\www\woocommerce\
git prune
git gc
git fetch

cd w:\www\wp-acacia\
git prune
git gc
git fetch

cd w:\www\wp-base\
git prune
git gc
git fetch

cd w:\www\wp-browns\
git prune
git gc
git fetch

cd w:\www\wp-forartssake\
git prune
git gc
git fetch

cd w:\www\wp-intothezoneonline\
git prune
git gc
git fetch

cd w:\www\wp-orchid\
git prune
git gc
git fetch

cd w:\www\wp-ripeandready\
git prune
git gc
git fetch

cd w:\www\wp-ruci\
git prune
git gc
git fetch

cd w:\www\wp-supportrefugees\
git prune
git gc
git fetch

cd w:\www\wp-susanrichardsonwriter\
git prune
git gc
git fetch

cd w:\www\wp-terranostraschool\
git prune
git gc
git fetch

cd w:\www\wp-ukstag\
git prune
git gc
git fetch

cd w:\www\wp-westlondontutoring\
git prune
git gc
git fetch

toast.bat "Done Cleainig up Repos"
