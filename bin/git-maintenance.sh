ROOTDIR=$PWD;

for D in *; do
    echo "${ROOTDIR}/${D}";
    cd "${ROOTDIR}/${D}";
    if [ -d "${ROOTDIR}/${D}/.git" ]; then
		git status
		git prune
		git gc
		git fetch
    fi
done

toast.bat "Done Cleainig up Repos"
