# To add:
# Setup CRON to run in Windows: https://codepre.com/how-to-start-cron-automatically-in-wsl-on-windows-10-and-11.html
# Setup backup cron
# 

sudo apt update -y
sudo apt upgrade -y

sudo add-apt-repository ppa:ondrej/php
sudo apt-get update

# "From: https://docs.microsoft.com/en-us/windows/dev-environment/javascript/nodejs-on-wsl : "
# Install NPM
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
bash
nvm install --lts

# "And install a bunch of ubuntu/php things"
# npm needs 'make' and friends
sudo apt install build-essential -y

# Install PHP7 and symlink it so we can use old php too
sudo apt install php7.4 -y
sudo apt install php7.4-mysql php7.4-mbstring php7.4-xml php7.4-gd php7.4-curl -y
sudo ln -s /usr/bin/php7.4 /usr/bin/php7

sudo apt install php8.0 -y
sudo apt install php8.0-mysql php8.0-mbstring php8.0-xml php8.0-gd php8.0-curl php8.0-zip -y

sudo apt-get install -y git-flow

# install composer: https://getcomposer.org/download/

git config --global user.email "sami@patabugen.co.uk"
git config --global user.name "Sami Greenbury"